'use strict'
const mongoose=require('mongoose')
const apps = require('./apps')
const config = require('./config')
const stringsResource = require('./resources/strings')

mongoose.connect(config.db);
var db = mongoose.connection;

db.on('error',function(){console.log(stringsResource.getStringResource(3))});

db.once('open', function() {
  console.log(stringsResource.getStringResource(2))
});

apps.apiV1.listen(config.apiV1Port, () => {
  console.log(stringsResource.getStringResource(4)+config.apiV1Port);
})
