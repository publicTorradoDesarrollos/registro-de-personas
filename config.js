'use strict'

module.exports ={
  version_number: process.env.VERSION_NUMBER,
  defaultPort:process.env.DEFAULT_PORT,
  apiV1Port: process.env.API_V1_PORT,
  db: process.env.MONGODB,
  exp_token_days:process.env.EXP_TOKEN,
  enabled_cors:process.env.CORS_ENABLE,
  secret_hash_token: process.env.SECRET_HASH_TOKEN,
}