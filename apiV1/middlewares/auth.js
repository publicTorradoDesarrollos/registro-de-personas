'use strict'

const Services = require('../services')
const HttpMessaje = require('../resources/http_messages')
const UserModel = require('../models/user')

function isAuth(req,res,next){
  try{
    if(req.headers.authorization.indexOf("Bearer")==-1){
      return res.status(400).send(HttpMessaje.getMessageFromStatusCode(400))  
    }
    var token = req.headers.authorization.replace("Bearer ","")
    token = token.replace("Bearer","")
  }catch(error){
    return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
  }
  if(!token){
    return res.status(403).send(HttpMessaje.getMessageFromStatusCode(403))
  }
  Services.decodeToken(token)
  .then(response => {
    var username = response
    let user = UserModel.UserModel.findOne({username:username},function(error,user){
      if(error){
        return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
      }
      if(!user){
        return res.status(401).send(HttpMessaje.getMessageFromStatusCode(401))
      }else{
        next()
      }
    })
  })
  .catch(response =>{
    res.status(response.status).send(response.message)
  })
}

module.exports = {
  isAuth
}
