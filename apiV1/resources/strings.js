'use strict'

const strings = {
  'default':{
    1: 'Generic message',
    2: 'Conexión con la base de datos establecida...',
    3: 'Conexión con la base de datos falló...',
    4: 'APP-Server está escuchando en el puerto:',
    5: 'Nuevo IOT disponible',
    6: 'Hace click en la notifiacion para habilitarlo'
  }
}

function getStringResource(code,lang){
  if (code || !(Object.keys(strings).indexOf(lang) == -1 ))
    lang = 'default'
  if (!strings[lang][code]){
    code = 999
  }
  return strings[lang][code];
}


module.exports = {
  getStringResource
}
