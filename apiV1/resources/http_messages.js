'use strict'

const status_codes = {
  'default':{
    999: {'message': 'Generic message'},
    200: {'message':'OK'}, //Success!
    304: {'message':'Not Modified'}, //There was no new data to return.
    400: {'message':'Bad Request'}, //The request was invalid or cannot be otherwise served. An accompanying error message will explain further. Requests without authentication are considered invalid and will yield this response.
    401: {'message':'Unauthorized'}, //Missing or incorrect authentication credentials. Also returned in other circumstances (for example, all calls to API v1 endpoints return 401).
    403: {'mesage':'Forbidden'},//The request is understood, but it has been refused or access is not allowed. An accompanying error message will explain why. This code is used when requests are being denied due to update limits . Other reasons for this status being returned are listed alongside the response codes in the table below.'
    404: {'mesage':'Not Found'},//The URI requested is invalid or the resource requested, such as a user, does not exists. Also returned when the requested format is not supported by the requested method.
    406: {'mesage':'Not Acceptable'},//Returned when an invalid format is specified in the request.
    410: {'mesage':'Gone'},//This resource is gone. Used to indicate that an API endpoint has been turned off.
    420: {'mesage':'Enhance Your Calm'},//Returned when an application is being rate limited .
    422: {'mesage':'Unprocessable Entity'},//Returned when an image uploaded to POST account / update_profile_banner is unable to be processed.
    429: {'mesage':'Too Many Requests'},//Returned when an image uploaded to POST account / update_profile_banner is unable to be processed.
    422: {'mesage':'Unprocessable Entity'},//Returned when a request cannot be served due to the application’s rate limit having been exhausted for the resource. See Rate Limiting .
    500: {'message':'Internal Server Error'},//Something is broken. Please post to the developer forums with additional details of your request, in case others are having similar issues.
    502: {'message':'Bad Gateway'},
    502: {'message':'Service Unavailable'},
    504: {'message':'Gateway timeout'}
  }

}
function getMessageFromStatusCode(code,lang){
  if (code || !(Object.keys(status_codes).indexOf(lang) == -1 ))
    lang = 'default'
  if (!status_codes[lang][code]){
    code = 999
  }
  return status_codes[lang][code];
}


module.exports = {
  getMessageFromStatusCode
}
