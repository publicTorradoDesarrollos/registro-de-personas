'use strict'

const express = require('express')
const auth = require('../middlewares/auth')

//Include of controllers
const Auth = require('../controllers/api/auth')
const Persona = require('../controllers/api/persona')

const api = express.Router()

  /**
   * @swagger
   * tags:
   *   - name: Auth
   *     description: Servicios Auth
   *   - name: Personas
   *     description: Servicios Personas
   */


//Users Endpoints
  /**
   * @swagger
   * /auth/signup:
   *   post:
   *     summary: Signup user
   *     description:
   *     tags:
   *      - Auth
   *     consumes:
   *      - application/json
   *     produces:
   *      - application/json
   *     parameters:
   *      - in: "body"
   *        name: "body"
   *        description: "Enviar informacion sobre el user para signup"
   *        required: true
   *        schema:
   *          $ref: "#/definitions/UserSignUp"
   *     responses:
   *       200:
   *         description: Signup successful
   *       400:
   *         description: Bad request
   *       401:
   *         description: Token invalid
   *       403:
   *         description: Require token
   *       500:
   *         description: Internal server error
   */
  api.post('/auth/signup', Auth.postUsersControl)


//Login
  /**
   * @swagger
   * /auth/login:
   *   post:
   *     summary: Login user
   *     description: Retorna un token si el login fue satisfactorio
   *     tags:
   *      - Auth
   *     consumes:
   *      - application/json
   *     produces:
   *      - application/json
   *     parameters:
   *      - in: "body"
   *        name: "body"
   *        description: "Enviar el username y el password"
   *        required: true
   *        schema:
   *          $ref: "#/definitions/UserLogin"
   *     responses:
   *       200:
   *         description: Login successfull
   *       401:
   *         description: User or password invalid
   *       500:
   *         description: Internal server error
   */
  api.post('/auth/login', Auth.postLoginUserControl )





//Personas Endpoints
  /**
   * @swagger
   * /personas:
   *   post:
   *     summary: Carga de persona
   *     description:
   *     tags:
   *      - Personas
   *     consumes:
   *      - application/json
   *     produces:
   *      - application/json
   *     parameters:
   *      - in: "header"
   *        name: "Authorization"
   *        description: "Bearer + Token"
   *        required: true
   *        schema:
   *          required: true
   *          type: "string"
   *        example: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YzVmNTUxYzUyZjkzNjM1ZjE4NDY1N2YiLCJpYXQiOjE1NTIyNDIwOTgsImV4cCI6MTU1MzQ1MTY5OH0.5aNTEG5okjsdjJataU2ad08u3YoZkHjnJLVjY1tYJZQ"
   *      - in: "body"
   *        name: "body"
   *        description: "Informacion sobre la alta de persona"
   *        required: true
   *        schema:
   *          $ref: "#/definitions/PersonaSub"
   *     responses:
   *       200:
   *         description: Successful
   *       400:
   *         description: Bad request
   *       401:
   *         description: Token invalid
   *       403:
   *         description: Require token
   *       500:
   *         description: Internal server error
   */
  api.post('/personas',auth.isAuth, Persona.postPersonaControl)
  /**
   * @swagger
   * /personas/{personaId}:
   *   delete:
   *     summary: Borrar persona
   *     description:
   *     tags:
   *      - Personas
   *     consumes:
   *      - application/json
   *     produces:
   *      - application/json
   *     parameters:
   *      - in: "path"
   *        name: "personaId"
   *        description: "personaId"
   *        required: true
   *        type: "string"
   *      - in: "header"
   *        name: "Authorization"
   *        description: "Bearer + Token"
   *        required: true
   *        schema:
   *          required: true
   *          type: "string"
   *        example: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YzVmNTUxYzUyZjkzNjM1ZjE4NDY1N2YiLCJpYXQiOjE1NTIyNDIwOTgsImV4cCI6MTU1MzQ1MTY5OH0.5aNTEG5okjsdjJataU2ad08u3YoZkHjnJLVjY1tYJZQ"
   *     responses:
   *       200:
   *         description: Successful
   *       400:
   *         description: Bad request
   *       401:
   *         description: Token invalid
   *       403:
   *         description: Require token
   *       500:
   *         description: Internal server error
   */
api.delete('/personas/:personaId',auth.isAuth, Persona.deletePersonaControl)
  /**
   * @swagger
   * /personas/{personaId}:
   *   put:
   *     summary: Modificar datos persona
   *     description:
   *     tags:
   *      - Personas
   *     consumes:
   *      - application/json
   *     produces:
   *      - application/json
   *     parameters:
   *      - in: "path"
   *        name: "personaId"
   *        description: "PersonaId"
   *        required: true
   *        type: "string"
   *      - in: "header"
   *        name: "Authorization"
   *        description: "Bearer + Token"
   *        required: true
   *        schema:
   *          required: true
   *          type: "string"
   *        example: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YzVmNTUxYzUyZjkzNjM1ZjE4NDY1N2YiLCJpYXQiOjE1NTIyNDIwOTgsImV4cCI6MTU1MzQ1MTY5OH0.5aNTEG5okjsdjJataU2ad08u3YoZkHjnJLVjY1tYJZQ"
   *      - in: "body"
   *        name: "body"
   *        description: "Send only information about key to modify in the user"
   *        required: true
   *        schema:
   *          $ref: "#/definitions/PersonaSub"
   *     responses:
   *       200:
   *         description: Successful
   *       400:
   *         description: Bad request
   *       401:
   *         description: Token invalid
   *       403:
   *         description: Require token
   *       404:
   *         description: User not found
   *       500:
   *         description: Internal server error
   */
api.put('/personas/:personaId', auth.isAuth, Persona.modifyPersonaControl)
  /**
   * @swagger
   * /personas/{personaId}:
   *   get:
   *     summary: Obtener information sobre la persona
   *     description:
   *     tags:
   *      - Personas
   *     consumes:
   *      - application/json
   *     produces:
   *      - application/json
   *     parameters:
   *      - in: "path"
   *        name: "personaId"
   *        description: "personaId"
   *        required: true
   *        type: "string"
   *      - in: "header"
   *        name: "Authorization"
   *        description: "Bearer + Token"
   *        required: true
   *        schema:
   *          required: true
   *          type: "string"
   *        example: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YzVmNTUxYzUyZjkzNjM1ZjE4NDY1N2YiLCJpYXQiOjE1NTIyNDIwOTgsImV4cCI6MTU1MzQ1MTY5OH0.5aNTEG5okjsdjJataU2ad08u3YoZkHjnJLVjY1tYJZQ"
   *     responses:
   *       200:
   *         description: Successful
   *       400:
   *         description: Bad request
   *       401:
   *         description: Token invalid
   *       403:
   *         description: Require token
   *       404:
   *         description: User not found
   *       500:
   *         description: Internal server error
   */
api.get('/personas/:personaId', auth.isAuth, Persona.getPersonaControl)
  /**
   * @swagger
   * /personas:
   *   get:
   *     summary: Obtener informacion sobre todas las personas
   *     description:
   *     tags:
   *      - Personas
   *     consumes:
   *      - application/json
   *     produces:
   *      - application/json
   *     parameters:
   *      - in: "header"
   *        name: "Authorization"
   *        description: "Bearer + Token"
   *        required: true
   *        schema:
   *          required: true
   *          type: "string"
   *        example: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YzVmNTUxYzUyZjkzNjM1ZjE4NDY1N2YiLCJpYXQiOjE1NTIyNDIwOTgsImV4cCI6MTU1MzQ1MTY5OH0.5aNTEG5okjsdjJataU2ad08u3YoZkHjnJLVjY1tYJZQ"
   *     responses:
   *       200:
   *         description: Successful
   *       400:
   *         description: Bad request
   *       401:
   *         description: Token invalid
   *       403:
   *         description: Require token
   *       404:
   *         description: There are not users
   *       500:
   *         description: Internal server error
   */
api.get('/personas', auth.isAuth, Persona.getAllPersonasControl)




module.exports = api