'use strict'
const jwt = require('jwt-simple')
const moment = require('moment')
const bcrypt = require('bcrypt-nodejs')
const crypto = require('crypto')
const hash = require('object-hash')
const config =  require('../../config')
const HttpMessaje = require('../resources/http_messages')

function createToken (user){
  const payload = {
    sub: user.username,
    iat: moment().unix(),
    exp: moment().add(config.exp_token_days,'days').unix()
  }
  return jwt.encode(payload,config.secret_hash_token)
}

function decodeToken (token){
  const decoded = new Promise((resolve,reject) =>{
    try{
      const playload = jwt.decode(token, config.secret_hash_token)
      if(playload.exp < moment().unix()){
        reject({
          status: 401,
          message: "Expired token"
        })
      }
      resolve(playload.sub)
    }catch(err){
      reject({
        status: 401,
        message: HttpMessaje.getMessageFromStatusCode(401)
      })
    }
  })
  return decoded
}

function encrypt(data){
 const encryptPromise = new Promise((resolve,reject) =>{
   try{
     bcrypt.genSalt(10, (err,salt) => {
       if (err) reject(err)
       bcrypt.hash(data,salt,null, (err,hash) =>{
         if (err) reject(err)
         resolve(hash);
       })
     })
   }catch(err){
     reject(err)
   }

 })
 return encryptPromise;
}

function compareHash(data,hash){
 const comparePromise = new Promise((resolve,reject) =>{
     bcrypt.compare(data, hash, (err, result) => {
    	            if (err) {
    	               reject (err)
    	            } else {
    	               resolve(result)
                  }
     })
 })
 return comparePromise;
}

//Filter the keys of Json file
function filterJson(data,filters){
    var newData = {}
    for( var id in filters){
    if(data[filters[id]] != undefined)
    newData[filters[id]] = data[filters[id]]
    }
    return newData;
 }


 function generatePersonaIdCode(dni){
  const promise = new Promise((resolve,reject) => {
    try{
      var payload = {dni : dni}
      resolve(hash(payload))
    }catch(err){
      reject(err)
    }
  })
  return promise
}



module.exports = {
  createToken,
  decodeToken,
  encrypt,
  compareHash,
  filterJson,
  generatePersonaIdCode
}