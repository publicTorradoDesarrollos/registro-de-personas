'use strict'
const HttpMessaje = require('../../resources/http_messages')
const PersonaModel = require('../../models/persona')
const Services=require('../../services/index')

const PersonaFilters=["nombre", "apellido", "dni"]

const PersonaFilters2=["personaId","nombre", "apellido", "dni"]

//Crear Persona
function postPersonaControl (req,res){
  var newData =  PersonaModel.PersonaModel(req.body)
  Services.generatePersonaIdCode(newData.dni).then((result) => {
    newData.personaId = result 
    newData.created = Date()
    newData.modified = ""
    newData.save(function(error){
      if(error){
        return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
      }else{
        return res.status(200).send(Services.filterJson(newData,PersonaFilters))
      }
    })      
  })
  .catch((error) => {
    return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
  })
}

function deletePersonaControl(req,res){
 var personaId = req.params.personaId

 PersonaModel.PersonaModel.deleteOne({personaId:personaId},function(error){
    if(error){
      return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
    }
    }).then(result => {
        return res.status(200).send({})   
   })
}

function modifyPersonaControl(req,res){
  var personaId = req.params.personaId
  var dataToEdit = req.body

  dataToEdit.modified = Date()

  var personaUpdate = PersonaModel.PersonaModel.findOneAndUpdate(
       { personaId : personaId },
       { $set : dataToEdit },
       { new : true },
    function(error,personaUpdate){
      if(error){
        return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
      }else{
        if(!personaUpdate){
          return res.status(404).send(HttpMessaje.getMessageFromStatusCode(404))
        }else{
          return res.status(200).send(Services.filterJson(personaUpdate,PersonaFilters))
        }
      }
   })
}

function getPersonaControl(req,res){
  var personaId = req.params.personaId

  var persona = PersonaModel.PersonaModel.findOne({personaId:personaId},function(error,persona){
      if(error){
        return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
      }
      if(!persona){
        return res.status(404).send(HttpMessaje.getMessageFromStatusCode(404))
      }else{
        return res.status(200).send(Services.filterJson(persona,PersonaFilters))
      }
  })
}

function getAllPersonasControl(req,res){

  var dataTosend=[]

  var persona=PersonaModel.PersonaModel.find({},function(error,persona){
      if(error){
        return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
      }
      if(!persona){
        return res.status(404).send(HttpMessaje.getMessageFromStatusCode(404))
      }else{
        for(var key in persona){
          dataTosend[key]=Services.filterJson(persona[key],PersonaFilters2)
        }
        return res.status(200).send({"personas":dataTosend})
      }
  })
}



module.exports ={
  postPersonaControl,
  deletePersonaControl,
  modifyPersonaControl,
  getPersonaControl,
  getAllPersonasControl
}
