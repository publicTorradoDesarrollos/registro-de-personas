'use strict'
const HttpMessaje = require('../../resources/http_messages')
const UserModel = require('../../models/user')
const Services=require('../../services/index')

const UserFilters=["username"]

//Create user
function postUsersControl (req,res){
    var newData =  UserModel.UserModel(req.body)
    Services.encrypt(newData.password).then((result) => {
      newData.password = result
      newData.created = Date()
      newData.modified = ""
      newData.save(function(error){
        if(error){
          return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
        }else{
          return res.status(200).send({"token":Services.createToken(newData), "user":Services.filterJson(newData,UserFilters) })
        }
      })      
    })
    .catch((error) => {
      return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
    })
  }
  

  function postLoginUserControl(req,res){

    var username = req.body.username
    var password = req.body.password
  
    var user = UserModel.UserModel.findOne({username:username},function(error,user){
      if(error){
        return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
      }
      if(!user){
        return res.status(401).send(HttpMessaje.getMessageFromStatusCode(401))
      }
      Services.compareHash(password,user.password).then((result) => {
        if(result){
           return res.status(200).send({"token":Services.createToken(user), "user":Services.filterJson(user,UserFilters) })
        }
        else{
          return res.status(401).send(HttpMessaje.getMessageFromStatusCode(401))
        }
      })
      .catch((error) => {
        return res.status(500).send(HttpMessaje.getMessageFromStatusCode(500))
      })
    })
  }




module.exports ={
    postUsersControl,
    postLoginUserControl

  }
  