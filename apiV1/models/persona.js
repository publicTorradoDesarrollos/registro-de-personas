'use strict'
const mongose = require('mongoose')

  /**
   * @swagger
   * definitions:
   *   Persona:
   *     title: "Persona"
   *     properties:
   *         personaId:
   *           type: "string"
   *           example: "Pablo"
   *         nombre:
   *           type: "string"
   *           example: "Pablo"
   *         apellido:
   *           type: "string"
   *           example: "Torrado"
   *         dni:
   *           type: "string"
   *           example: "112233445566"
   *     required:
   *         - "personaId" 
   *         - "nombre"
   *         - "apellido"
   *         - "dni"
   */



  /**
   * @swagger
   * definitions:
   *   PersonaSub:
   *     title: "PersonaPost"
   *     properties:
   *         nombre:
   *           type: "string"
   *           example: "Pablo"
   *         apellido:
   *           type: "string"
   *           example: "Torrado"
   *         dni:
   *           type: "string"
   *           example: "112233445566"
   *     required:
   *         - "personaId" 
   *         - "nombre"
   *         - "apellido"
   *         - "dni"
   */


const PersonaSchema = new mongose.Schema(
  {
      "personaId": {
       "type": "String"
      },
      "nombre": {
        "type": "String"
      },
      "apellido": {
        "type": "String"
      },
      "dni": {
        "type": "String",
        "unique":true
      },
      "created": {
        "type": "String",
        "default":Date().toString()
      },
      "modified": {
        "type": "String",
        "default":Date().toString()
      }
});

const PersonaModel = mongose.model('PersonaModel', PersonaSchema);

module.exports ={
  PersonaModel
}