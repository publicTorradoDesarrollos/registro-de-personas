'use strict'
const mongose = require('mongoose')

  /**
   * @swagger
   * definitions:
   *   User:
   *     title: "User"
   *     properties:
   *         username:
   *           type: "string"
   *           example: "Pablo"
   *         password:
   *           type: "string"
   *           example: "123456"
   *     required:
   *         - "username"
   *         - "password"
   */


 /**
   * @swagger
   * definitions:
   *   UserSignUp:
   *     title: "UserSignUp"
   *     properties:
   *         username:
   *           type: "string"
   *           example: "Pablo"
   *         password:
   *           type: "string"
   *           example: "123456"
   *     required:
   *         - "username"
   *         - "password"
   */


 /**
   * @swagger
   * definitions:
   *   UserLogin:
   *     title: "UserLogin"
   *     properties:
   *         username:
   *           type: "string"
   *           example: "Pablo"
   *         password:
   *           type: "string"
   *           example: "123456"
   *     required:
   *         - "username"
   *         - "password"
   */


const UserSchema = new mongose.Schema(
  {
      "username": {
        "type": "String",
        "unique": true
      },
      "password": {
        "type": "String"
      },
      "created": {
        "type": "String",
        "default":Date().toString()
      },
      "modified": {
        "type": "String",
        "default":Date().toString()
      }
});

const UserModel = mongose.model('UserModel', UserSchema);

module.exports ={
    UserModel
}