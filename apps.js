const url = require('url')
const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const config = require('./config')
const api_v1 = require('./apiV1/routes/api')
const swaggerSpec = require('./swagger.js');

const apiV1 = express()

apiV1.use(bodyParser.urlencoded({extended: false}))
apiV1.use(bodyParser.json())

apiV1.get('/', (req,res) =>{
  res.redirect('/api/v1/docs');
})


apiV1.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

//Habilita el cors para todos los dominios
if( config.enabled_cors ){
  const cors = require('cors')
  apiV1.use(cors()); // Enable CORS
  apiV1.all('*', cors(), function(req, res, next){
    next();
  });
}

apiV1.use("/api/v1/docs", express.static(path.join(path.join(__dirname, 'apiV1'),'docs')));
apiV1.use('/api/v1',api_v1)

module.exports = {
  apiV1
}
