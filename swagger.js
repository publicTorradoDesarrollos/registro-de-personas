const swaggerJSDoc = require('swagger-jsdoc');
// swagger definition
var swaggerDefinition = {
  info: {
    title: 'AppServer-REST-API-Registro de personas-Documentacion',
    version: '1.0.0',
    description: 'Documentacion de AppServer-REST-API-1-Registro de personas',
  },
  host: '',
  basePath: '/api/v1',
};

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./apiV1/models/*.js','./apiV1/routes/*.js','./apiV1/resources/http_messages.js']
};

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);
module.exports = swaggerSpec
